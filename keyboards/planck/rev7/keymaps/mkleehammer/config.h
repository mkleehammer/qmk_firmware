/* Copyright 2015-2023 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#undef DEBOUNCE
#define DEBOUNCE 10
/* I've been getting some chatter, particularly on the 'e' key like the blog mentions, so I'll
   double the debounce time from the default 5ms.

   https://getreuer.info/posts/keyboards/faqs/index.html?utm_source=pocket_saves#pressing-a-key-sometimes-types-it-twice
   https://docs.qmk.fm/#/feature_debounce_type */

/*  https://docs.qmk.fm/using-qmk/software-features/tap_hold  */


// Choral hold will disable home row mods when used with keys on the same side of the keyboard,
// ensuring rolls don't turn into accidental triggers of modifiers.  This is even more
// important if you use a non-QWERTY layout with more rolls.
//
// I found out the hard way that HOLD_ON_OTHER_KEY_PRESS is terrible for me.  I would get
// almost one accidental HMR per word!  Stick with PERMISSIVE_HOLD.
//
// It was also necessary for me to bump the TAPPING_TERM way up.  The recommendation comes from
// the author in this comment:
// https://www.reddit.com/r/ErgoMechKeyboards/comments/1j0cjb7/comment/mfcsnvz/
//
// I think a combo will resolve to a hold as soon as the timer runs out and I am using HMRs
// very slowly.  I may need to try the default behavior again.

#define CHORDAL_HOLD
#define PERMISSIVE_HOLD
#define TAPPING_TERM 300


#define CAPS_WORD_IDLE_TIMEOUT 3000  // Turn off Caps Word after 3 seconds.  Default is 5
#define DOUBLE_TAP_SHIFT_TURNS_ON_CAPS_WORD


#ifdef AUDIO_ENABLE
#define AUDIO_INIT_DELAY
/*  #    define STARTUP_SONG SONG(PLANCK_SOUND)  */
// #define STARTUP_SONG SONG(NO_SOUND)
#define STARTUP_SONG SONG(STARTUP_SOUND)
/*
#define DEFAULT_LAYER_SONGS { SONG(QWERTY_SOUND), \
                              SONG(COLEMAK_SOUND), \
                              SONG(DVORAK_SOUND) \
                            }
*/
#endif

// Cut down on the number of effects to save memory.
#ifdef RGBLIGHT_ENABLE
#define RGBLIGHT_EFFECT_BREATHING
#define RGBLIGHT_EFFECT_TWINKLE
#endif

