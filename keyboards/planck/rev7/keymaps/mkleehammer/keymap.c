/* Copyright 2015-2023 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

// #include "print.h"


enum planck_layers { _QWERTY, _COLEMAKDH, _TARMAK2, _LOWER, _RAISE, _ADJUST, _FUNCTION, _NAV };

#define LOWER MO(_LOWER)
#define RAISE MO(_RAISE)
#define FKEYS MO(_FUNCTION)

#define KC_CAD	LALT(LCTL(KC_DEL))
#define KC_AF4  LALT(KC_F4)

enum planck_keycodes {
  QWERTY = SAFE_RANGE,
  TARMAK2,
  COLEMAKDH,
};


/* clang-format off */
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_QWERTY] = LAYOUT_planck_grid(
 KC_TAB,  KC_Q,          KC_W,        KC_E,         KC_R,        KC_T,   KC_Y,   KC_U,        KC_I,         KC_O,        KC_P,    KC_BSPC,
 KC_ESC,  LT(_NAV,KC_A), CTL_T(KC_S), LALT_T(KC_D), SFT_T(KC_F), KC_G,   KC_H,   SFT_T(KC_J), LALT_T(KC_K), CTL_T(KC_L), KC_SCLN, KC_QUOT,
 KC_LSFT, KC_Z,          KC_X,        KC_C,         KC_V,        KC_B,   KC_N,   KC_M,        KC_COMM,      KC_DOT,      KC_SLSH, KC_ENT,
 KC_LCTL, FKEYS,         KC_NO,       KC_LGUI,      LOWER,       KC_SPC, KC_SPC, RAISE,       KC_LEFT,      KC_DOWN,     KC_UP,   KC_RGHT
),

/* https://dreymar.colemak.org/tarmak-steps.html */
/* Tarmak1
[_TARMAK1] = LAYOUT_planck_grid(
 KC_TAB,  KC_Q,          KC_W,        KC_J,         KC_R,        KC_T,   KC_Y,   KC_U,        KC_I,         KC_O,        KC_P,    KC_BSPC,
 KC_ESC,  LT(_NAV,KC_A), CTL_T(KC_S), LALT_T(KC_D), SFT_T(KC_F), KC_G,   KC_H,   SFT_T(KC_N), LALT_T(KC_E), CTL_T(KC_L), KC_SCLN, KC_QUOT,
s KC_LSFT, KC_Z,          KC_X,        KC_C,         KC_V,        KC_B,   KC_K,   KC_M,        KC_COMM,      KC_DOT,      KC_SLSH, KC_ENT,
 KC_LCTL, FKEYS,         KC_NO,       KC_LGUI,      LOWER,       KC_SPC, KC_SPC, RAISE,       KC_LEFT,      KC_DOWN,     KC_UP,   KC_RGHT
),
*/


/* Tarmak-DH #2 */

/* https://dreymar.colemak.org/tarmak-steps.html#tmk-dh

   Look for Tarmak Curl(DH)Angle-ANSI: / 2b */

/*  [_TARMAK2] = LAYOUT_planck_grid(  */
/*   KC_TAB,  KC_Q,          KC_W,        KC_F,         KC_R,        KC_B,   KC_Y,   KC_U,        KC_I,         KC_O,        KC_P,    KC_BSPC,  */
/*   KC_ESC,  LT(_NAV,KC_A), CTL_T(KC_S), LALT_T(KC_D), SFT_T(KC_T), KC_G,   KC_M,   SFT_T(KC_N), LALT_T(KC_E), CTL_T(KC_L), KC_SCLN, KC_QUOT,  */
/*   KC_LSFT, KC_Z,          KC_X,        KC_C,         KC_V,        KC_J,   KC_K,   KC_H,        KC_COMM,      KC_DOT,      KC_SLSH, KC_ENT,  */
/*   KC_LCTL, FKEYS,         KC_NO,       KC_LGUI,      LOWER,       KC_SPC, KC_SPC, RAISE,       KC_LEFT,      KC_DOWN,     KC_UP,   KC_RGHT  */
/*  ),  */

[_TARMAK2] = LAYOUT_planck_grid(
 KC_TAB,  KC_Q,          KC_W,        KC_F,         KC_R,        KC_G,   KC_Y,   KC_U,        KC_I,         KC_O,        KC_P,    KC_BSPC,
 KC_ESC,  LT(_NAV,KC_A), CTL_T(KC_S), LALT_T(KC_D), SFT_T(KC_T), KC_J,   KC_H,   SFT_T(KC_N), LALT_T(KC_E), CTL_T(KC_L), KC_SCLN, KC_QUOT,
 KC_LSFT, KC_Z,          KC_X,        KC_C,         KC_V,        KC_B,   KC_K,   KC_M,        KC_COMM,      KC_DOT,      KC_SLSH, KC_ENT,
 KC_LCTL, FKEYS,         KC_NO,       KC_LGUI,      LOWER,       KC_SPC, KC_SPC, RAISE,       KC_LEFT,      KC_DOWN,     KC_UP,   KC_RGHT
),

/*  Colemak-DH  */
/*  ,-----------------------------------------------------------------------------------.  */
/*  | Tab  |   Q  |   W  |   F  |   P  |   B  |   J  |   L  |   U  |   Y  |   ;  | Bksp |  */
/*  |------+------+------+------+------+------+------+------+------+------+------+------|  */
/*  | Esc  |   A  |   R  |   S  |   T  |   G  |   M  |   N  |   E  |   I  |   O  |  '   |  */
/*  |------+------+------+------+------+------+------+------+------+------+------+------|  */
/*  | Shift|   Z  |   X  |   C  |   D  |   V  |   K  |   H  |   ,  |   .  |   /  |Enter |  */
/*  |------+------+------+------+------+------+------+------+------+------+------+------|  */
/*  | Ctrl | Fn   |  -   | GUI  |Lower |    Space    |Raise | Left | Down |  Up  |Right |  */
/*  `-----------------------------------------------------------------------------------'  */
[_COLEMAKDH] = LAYOUT_planck_grid(
 KC_TAB,  KC_Q,          KC_W,        KC_F,         KC_P,        KC_B,   KC_J,   KC_L,        KC_U,         KC_Y,        KC_SCLN, KC_BSPC,
 KC_ESC,  LT(_NAV,KC_A), CTL_T(KC_R), LALT_T(KC_S), SFT_T(KC_T), KC_G,   KC_M,   SFT_T(KC_N), LALT_T(KC_E), CTL_T(KC_I), KC_O,    KC_QUOT,
 KC_LSFT, KC_Z,          KC_X,        KC_C,         KC_D,        KC_V,   KC_K,   KC_H,        KC_COMM,      KC_DOT,      KC_SLSH, KC_ENT ,
 KC_LCTL, FKEYS,         KC_NO,       KC_LGUI,      LOWER,       KC_SPC, KC_SPC, RAISE,       KC_LEFT,      KC_DOWN,     KC_UP,   KC_RGHT
),




/*  Colemak  */
/*  ,-----------------------------------------------------------------------------------.  */
/*  | Tab  |   Q  |   W  |   F  |   P  |   G  |   J  |   L  |   U  |   Y  |   ;  | Bksp |  */
/*  |------+------+------+------+------+------+------+------+------+------+------+------|  */
/*  | Esc  |   A  |   R  |   S  |   T  |   D  |   H  |   N  |   E  |   I  |   O  |  '   |  */
/*  |------+------+------+------+------+------+------+------+------+------+------+------|  */
/*  | Shift|   Z  |   X  |   C  |   V  |   B  |   K  |   M  |   ,  |   .  |   /  |Enter |  */
/*  |------+------+------+------+------+------+------+------+------+------+------+------|  */
/*  | Ctrl | Fn   |  -   | GUI  |Lower |    Space    |Raise | Left | Down |  Up  |Right |  */
/*  `-----------------------------------------------------------------------------------'
[_COLEMAK] = LAYOUT_planck_grid(
 KC_TAB,  KC_Q,          KC_W,        KC_F,         KC_P,        KC_G,   KC_J,   KC_L,        KC_U,         KC_Y,        KC_SCLN, KC_BSPC,
 KC_ESC,  LT(_NAV,KC_A), CTL_T(KC_R), LALT_T(KC_S), SFT_T(KC_T), KC_D,   KC_H,   SFT_T(KC_N), LALT_T(KC_E), CTL_T(KC_I), KC_O,    KC_QUOT,
 KC_LSFT, KC_Z,          KC_X,        KC_C,         KC_V,        KC_B,   KC_K,   KC_M,        KC_COMM,      KC_DOT,      KC_SLSH, KC_ENT ,
 KC_LCTL, FKEYS,         KC_NO,       KC_LGUI,      LOWER,       KC_SPC, KC_SPC, RAISE,       KC_LEFT,      KC_DOWN,     KC_UP,   KC_RGHT
),
*/


/* In Emacs, M-{ and M-} move by paragraph.  I'm not consistent in pressing home row meta (Alt)
 * before raise/lower, so I've copied the toggle's to the raise and lower rows also.  The
 * macros don't work with '{' or '+' so I've left the macros off the right hand completely.
 * Fortunately the left keys all work and it is the left that I need for the Emacs keys.
 *
 * Unfortunately it is also making me miss '-' a lot.  I'm taking it off of the F key too.
 */

[_LOWER] = LAYOUT_planck_grid(
 KC_TILD, KC_EXLM, KC_AT,          KC_HASH,        KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_BSPC,
 KC_DEL,  KC_LBRC, CTL_T(KC_RBRC), LALT_T(KC_EQL), KC_MINS, KC_BSLS, KC_NO,   KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE,
 CW_TOGG, KC_NO,   _______,        KC_NO,          KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_LT,   KC_GT,   _______, _______,
 _______, _______, _______,        _______,        _______, _______, _______, _______, _______, _______, _______, _______
),


[_RAISE] = LAYOUT_planck_grid(
 KC_GRV,  KC_1,    KC_2,           KC_3,           KC_4,           KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_BSPC,
 KC_DEL,  KC_LBRC, CTL_T(KC_RBRC), LALT_T(KC_EQL), SFT_T(KC_MINS), KC_BSLS, KC_NO,   KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE,
 CW_TOGG, KC_NO,   _______,        KC_NO,          KC_NO,          KC_NO,   KC_NO,   KC_NO,   _______, _______, _______, _______,
 _______, _______, _______,        _______,        _______,        _______, _______, _______, _______, _______, _______, _______
),


[_ADJUST] = LAYOUT_planck_grid(

 //TAB,     KC_Q,    KC_W,    KC_E,    KC_R,             KC_T,    KC_Y,    KC_U,      KC_I,    KC_O,    KC_P,    KC_BSPC,
 KC_F18,    KC_MRWD, KC_MFFD, KC_MPLY, KC_NO,            QWERTY,  TARMAK2, COLEMAKDH, KC_NO,   AU_TOGG, EE_CLR,  QK_BOOT,
 KC_NO,     KC_VOLD, KC_VOLU, KC_MUTE, KC_NO,            KC_NO,   KC_NO,   KC_NO,     KC_NO,   RGB_TOG, KC_NO,   KC_NO,
 KC_CAPS,   KC_BRID, KC_BRIU, RGB_TOG, RGB_MODE_FORWARD, RGB_HUI, RGB_SAI, RGB_VAI,   RGB_VAD, KC_NO,   KC_NO,   _______,
 KC_CAD,    KC_AF4,  _______, _______, _______,          _______, _______, _______,   _______, _______, _______, _______
 //KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,             KC_B,    KC_N,    KC_M,      KC_COMM, KC_DOT,  KC_SLSH, KC_ENT ,
),

/* I've assigned F18 above which I don't normally use.  I'm going to assign using xbindkeys to
   run a program.  (I tried F20, but that toggles the microphone.) */


[_FUNCTION] = LAYOUT_planck_grid(
 _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  _______,
 _______, KC_F11,  KC_F12,  KC_F13,  KC_F14,  KC_F15,  KC_F16,  KC_F17,  KC_F18,  KC_F19,  KC_F20,  _______,
 _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
 _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
),


[_NAV] = LAYOUT_planck_grid(
 KC_NO, KC_NO, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_HOME, KC_PGDN, KC_PGUP, KC_END,  KC_UP,   KC_BSPC,
 KC_NO, KC_NO, KC_LCTL, KC_LALT, KC_LSFT, KC_NO,   KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, KC_NO,   KC_NO,
 KC_NO, KC_NO, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_DOWN, KC_NO,   KC_NO,   KC_NO,   KC_NO,   _______,
 KC_NO, KC_NO, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
),

/*
[_TKEY] = LAYOUT_planck_grid
(
 KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_TAB,  KC_7, KC_8,    KC_9,   KC_MINS, KC_BSPC,
 KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_4, KC_5,    KC_6,   KC_PLUS, KC_NO,
 KC_NO,   KC_NO,   KC_X,    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_1, KC_2,    KC_3,   KC_EQL,  KC_ENT,
 _______, _______, _______, _______, _______, _______, _______, KC_0, KC_COMM, KC_DOT, KC_ASTR, KC_SLSH
),
*/

};
/* clang-format on */


bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
  case QWERTY:
      if (record->event.pressed) {
          /*  print("mode just switched to qwerty and this is a huge string\n");  */
          set_single_persistent_default_layer(_QWERTY);
      }
      return false;
      break;
  case COLEMAKDH:
      if (record->event.pressed) {
          set_single_persistent_default_layer(_COLEMAKDH);
      }
      return false;
      break;
  case TARMAK2:
      if (record->event.pressed) {
          set_single_persistent_default_layer(_TARMAK2);
      }
      return false;
      break;
  case LOWER:
      if (record->event.pressed) {
          layer_on(_LOWER);
          update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
          layer_off(_LOWER);
          update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
  case RAISE:
      if (record->event.pressed) {
          layer_on(_RAISE);
          update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
          layer_off(_RAISE);
          update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
  }

  return true;
}


char chordal_hold_handedness(keypos_t key) {
    // Override whether a key is considered on the left or right for chordal.  We want anything
    // on the bottom row to be allowed with other keys on the same hand.  I'm not sure where
    // the default behavior is, but it works well with Raise & Lower, but not with Super which
    // I have on the bottom row.
    //
    // https://docs.qmk.fm/tap_hold#chordal-hold

    if (key.row == 3 || key.row == 7) {
        // The rows on the Planck are weird - maybe because I'm using the MIT setup with a 2U
        // spacebar.
        return '*';
    }

    // This is the default implementation of chordal_hold_handedness in
    // quantum/action_tapping.c.  I guess there is a chordal_hold_layout defined or constructed
    // for the Plank somewhere.  I printed out the values and they were L and R, so this
    // somehow works.
    return (char)pgm_read_byte(&chordal_hold_layout[key.row][key.col]);
}


bool caps_word_press_user(uint16_t keycode) {
    // Override the default to not shift on minus / hyphen.  Otherwise you can't enter an
    // underscore.

  switch (keycode) {
    // Keycodes that continue Caps Word, with shift applied.
    case KC_A ... KC_Z:
      add_weak_mods(MOD_BIT(KC_LSFT));  // Apply shift to the next key.
      return true;

    // Keycodes that continue Caps Word, without shifting.
    case KC_1 ... KC_0:
    case KC_BSPC:
    case KC_DEL:
    case KC_UNDS:
    case KC_MINS:
      return true;

    default:
      return false;  // Deactivate Caps Word.
  }
}

/* clang-format off */
float melody[8][2][2] = {
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}}, 
    {{440.0f, 8}, {440.0f, 24}},
};
/* clang-format on */

#define JUST_MINOR_THIRD 1.2
#define JUST_MAJOR_THIRD 1.25
#define JUST_PERFECT_FOURTH 1.33333333
#define JUST_TRITONE 1.42222222
#define JUST_PERFECT_FIFTH 1.33333333

#define ET12_MINOR_SECOND 1.059463
#define ET12_MAJOR_SECOND 1.122462
#define ET12_MINOR_THIRD 1.189207
#define ET12_MAJOR_THIRD 1.259921
#define ET12_PERFECT_FOURTH 1.33484
#define ET12_TRITONE 1.414214
#define ET12_PERFECT_FIFTH 1.498307	

deferred_token tokens[8];

uint32_t reset_note(uint32_t trigger_time, void *note) {
    *(float*)note = 440.0f;
    return 0;
}
