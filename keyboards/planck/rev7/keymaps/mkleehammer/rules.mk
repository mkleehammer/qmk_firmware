DEFERRED_EXEC_ENABLE = yes

# There is a bug that doesn't allow you to compile the rev7 without the encoder.
#
# https://github.com/qmk/qmk_firmware/pull/21213
#
#  ENCODER_ENABLE = no


BOOTMAGIC_ENABLE = no		# Virtual DIP switch configuration
# This seems to be required for the startup song, though I thought they got rid of Bootmagic.

RGBLIGHT_ENABLE = no

CAPS_WORD_ENABLE = yes

LTO_ENABLE = yes

# https://docs.qmk.fm/#/faq_debug
# CONSOLE_ENABLE = yes
